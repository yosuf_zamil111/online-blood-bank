package com.zamil.map.onlinebloodbank;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    Button loginButton,loginCancel;
    EditText loginEmail,loginPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        loginButton = (Button) findViewById(R.id.btnLogin);
        loginCancel = (Button) findViewById(R.id.btnCancel);
        loginCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(LoginActivity.this,HomeActivity.class);
                startActivity(out);
            }
        });

        final myDBClass myDb = new myDBClass(this);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loginEmail = (EditText) findViewById(R.id.loginEmail);
                loginPass = (EditText) findViewById(R.id.loginpassword);

                String email = loginEmail.getText().toString();
                String pass = loginPass.getText().toString();

                String storedPassword = myDb.getSinlgeEntry(email);

                if(pass.equals(storedPassword))
                {
                    Toast.makeText(LoginActivity.this, "Congrats: Login Successfully", Toast.LENGTH_LONG).show();
                    Intent ii=new Intent(LoginActivity.this,RegisteredDonarList.class);
                    startActivity(ii);
                }
                else
                if(pass.equals("")){
                    Toast.makeText(LoginActivity.this, "Please Enter Your Password", Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Password Incorrect", Toast.LENGTH_LONG).show();
                }
            }

        });


    }

}
