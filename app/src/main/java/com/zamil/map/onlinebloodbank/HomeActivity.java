package com.zamil.map.onlinebloodbank;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {
    Button aboutusbt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

      /*  aboutusbt = (Button) findViewById(R.id.about);
        aboutusbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(HomeActivity.this,AboutActivity.class);
                startActivity(out);
            }
        });*/
    }

    public void requestBlood(View v){
        Intent out = new Intent(HomeActivity.this,RegistrationActivity.class);
        startActivity(out);
    }

    public void donorList(View v){
        Intent out = new Intent(HomeActivity.this,VisitorDonarList.class);
        startActivity(out);
    }
    public void login(View v){
        Intent out = new Intent(HomeActivity.this,LoginActivity.class);
        startActivity(out);
    }
    public void regdonar(View v){
        Intent out = new Intent(HomeActivity.this,RegisteredDonarList.class);
        startActivity(out);
    }

    public void location(View v){
        Intent out = new Intent(HomeActivity.this,MapsActivity.class);
        startActivity(out);
    }
    public void aboutme(View v){
        Intent out = new Intent(HomeActivity.this,ContactActivity.class);
        startActivity(out);
    }


}
