package com.zamil.map.onlinebloodbank;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class RegistrationActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    Button save,cancel;
    Spinner sprCoun;
   // RadioGroup radioSexGroup;
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton1,radioSexButton2;
    String gender ="Male";
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        cancel = (Button) findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(RegistrationActivity.this, HomeActivity.class);
                startActivity(out);
            }
        });

        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SaveData()) {
                    Intent newActivity = new Intent(RegistrationActivity.this, LoginActivity.class);
                    startActivity(newActivity);
                }
            }

        });

        ctx = this;
        sprCoun = (Spinner) findViewById(R.id.spinner1);
        radioSexGroup=(RadioGroup)findViewById(R.id.radioGroup);
        //int selectedId = radioSexGroup.getCheckedRadioButtonId();
        radioSexGroup.setOnCheckedChangeListener(this);
        radioSexButton1=(RadioButton)findViewById(R.id.radioButton1);
        radioSexButton2=(RadioButton)findViewById(R.id.radioButton2);


    }

    @Override
    public void onCheckedChanged(RadioGroup group, int position) {
        // TODO Auto-generated method stub
        switch (position) {
            case R.id.radioButton1:
                gender = "Male";
                //System.out.println("Male");
                break;
            case R.id.radioButton2:
                gender = "Female";
                //System.out.println("Female");
                break;

            default:
                break;
        }
    }


    public boolean SaveData()
    {
        final EditText id = (EditText) findViewById(R.id.id);
        final EditText name = (EditText) findViewById(R.id.name);

        final TextView blood = (TextView) findViewById(R.id.bloodgroup);
        blood.setText(sprCoun.getSelectedItem().toString());

        final TextView sex = (TextView) findViewById(R.id.sex);
        sex.setText(gender);

        final EditText phone = (EditText) findViewById(R.id.phonenumber);
        final EditText currentlocation = (EditText) findViewById(R.id.currentlocation);
        final EditText email = (EditText) findViewById(R.id.email);
        final EditText pass = (EditText) findViewById(R.id.password);


        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        AlertDialog ad = adb.create();

        if(id.getText().length() == 0)
        {
            ad.setMessage("Please input [id] ");
            ad.show();
            id.requestFocus();
            return false;
        }
        if(name.getText().length() == 0)
        {
            ad.setMessage("Please input [name] ");
            ad.show();
            name.requestFocus();
            return false;
        }
       /* if(blood.getText().length() == 0)
        {
            ad.setMessage("Please input [blood group] ");
            ad.show();
            blood.requestFocus();
            return false;
        }*/
        /*if(sex.getText().length() == 0)
        {
            ad.setMessage("Please input [sex] ");
            ad.show();
            sex.requestFocus();
            return false;
        }*/
        if(phone.getText().length() == 0)
        {
            ad.setMessage("Please input [phone number] ");
            ad.show();
            phone.requestFocus();
            return false;
        }
        if(currentlocation.getText().length() == 0)
        {
            ad.setMessage("Please input [phone number] ");
            ad.show();
            currentlocation.requestFocus();
            return false;
        }
        if(email.getText().length() == 0)
        {
            ad.setMessage("Please input [email] ");
            ad.show();
            email.requestFocus();
            return false;
        }
        if(pass.getText().length() == 0)
        {
            ad.setMessage("Please input [password] ");
            ad.show();
            pass.requestFocus();
            return false;
        }

        final myDBClass myDb = new myDBClass(this);
        String arrData[] = myDb.SelectData(id.getText().toString());
        if(arrData != null)
        {
            ad.setMessage("ID already exists!  ");
            ad.show();
            id.requestFocus();
            return false;
        }

        long saveStatus = myDb.InsertData(id.getText().toString(),
                name.getText().toString(),
                blood.getText().toString(),
                sex.getText().toString(),
                phone.getText().toString(),
                currentlocation.getText().toString(),
                email.getText().toString(),
                pass.getText().toString());
        if(saveStatus <=  0)
        {
            ad.setMessage("Error!! ");
            ad.show();
            return false;
        }
        Toast.makeText(RegistrationActivity.this, "Add Data Successfully. ",
                Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


}
