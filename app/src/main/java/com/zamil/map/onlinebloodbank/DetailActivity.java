package com.zamil.map.onlinebloodbank;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DetailActivity extends AppCompatActivity {

    Button btnSignIn;
    final myDBClass myDb = new myDBClass(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Button cancel = (Button) findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(DetailActivity.this, HomeActivity.class);
                startActivity(out);
            }
        });

        Intent intent= getIntent();
        final String MemID = intent.getStringExtra("MemID");

        ShowData(MemID);

        //final myDBClass myDb = new myDBClass(this);

       /* btnSignIn = (Button) findViewById(R.id.btnUpdate);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               *//* if(UpdateData(MemID))
                {*//*
                Intent newActivity = new Intent(DetailActivity.this, UpdateActivity2.class);
                newActivity.putExtra("ID", MemID);
                startActivity(newActivity);
                //}
            }
        });*/

    }

    public void update(View v){
        final Dialog dialog = new Dialog(DetailActivity.this);
        dialog.setContentView(R.layout.login);
        dialog.setTitle("Login for Update");

        Intent intent= getIntent();
        final String MemID = intent.getStringExtra("MemID");

        final EditText editTextUserName=(EditText)dialog.findViewById(R.id.updateLoginEmail);
        final  EditText editTextPassword=(EditText)dialog.findViewById(R.id.updateLoginPass);

        Button btnSignIn=(Button)dialog.findViewById(R.id.buttonSignIn);

        btnSignIn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // get The User name and Password
                String userEmail=editTextUserName.getText().toString();
                String password=editTextPassword.getText().toString();

                String storedPassword=myDb.getSinlgeEntry(userEmail);

                // check if the Stored password matches with  Password entered by user
                if(password.equals(storedPassword))
                {
                    Toast.makeText(DetailActivity.this, "Congrats: Login Successfull", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                    Intent in = new Intent(DetailActivity.this,UpdateActivity2.class);
                    in.putExtra("ID", MemID);
                    startActivity(in);
                }
                else
                {
                    Toast.makeText(DetailActivity.this, "User Name or Password does not match", Toast.LENGTH_LONG).show();
                }
            }
        });

        dialog.show();
    }

    public void ShowData(String MemID)
    {
        final TextView tid = (TextView) findViewById(R.id.txtID);
        final TextView tname = (TextView) findViewById(R.id.txtName);
        final TextView tBlood = (TextView) findViewById(R.id.txtBlood);
        final TextView tsex = (TextView) findViewById(R.id.txtSex);
        final TextView tTel = (TextView) findViewById(R.id.txtTel);
        final TextView tlocation = (TextView) findViewById(R.id.txtLocation);
        final TextView temail = (TextView) findViewById(R.id.txtEmail);

        final myDBClass myDb = new myDBClass(this);

        String arrData[] = myDb.SelectData(MemID);

        if(arrData != null)
        {
            tid.setText(arrData[0]);
            tname.setText(arrData[1]);
            tBlood.setText(arrData[2]);
            tsex.setText(arrData[3]);
            tTel.setText(arrData[4]);
            tlocation.setText(arrData[5]);
            temail.setText(arrData[6]);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Close The Database
        myDb.close();
    }

}
