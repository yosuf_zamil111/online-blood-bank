package com.zamil.map.onlinebloodbank;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class UpdateActivity2 extends AppCompatActivity {

    Button okButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        Intent intent= getIntent();
        final String MemID = intent.getStringExtra("ID");

        ShowData(MemID);

        okButton = (Button) findViewById(R.id.btnUpdate1);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(UpdateData(MemID)) {
                    Intent out = new Intent(UpdateActivity2.this, RegisteredDonarList.class);
                    startActivity(out);
                }
            }
        });

    }
    public void ShowData(String MemID)
    {
        final TextView tMemberID = (TextView) findViewById(R.id.txtMemberID);
        final EditText tName = (EditText) findViewById(R.id.txtName);
        final EditText tBlood = (EditText) findViewById(R.id.txtBlood);
        final EditText tSex = (EditText) findViewById(R.id.txtSex);
        final EditText tPhone = (EditText) findViewById(R.id.txtPhone);
        final EditText tLocation = (EditText) findViewById(R.id.txtCurrentLocation);
        final EditText tEmail = (EditText) findViewById(R.id.txtEmail);


        final myDBClass myDb = new myDBClass(this);
        String arrData[] = myDb.SelectData(MemID);

        if(arrData != null)
        {
            tMemberID.setText(arrData[0]);
            tName.setText(arrData[1]);
            tBlood.setText(arrData[2]);
            tSex.setText(arrData[3]);
            tPhone.setText(arrData[4]);
            tLocation.setText(arrData[5]);
            tEmail.setText(arrData[6]);
        }
    }
    public boolean UpdateData(String MemID)
    {
        final TextView idShow = (TextView) findViewById(R.id.txtMemberID);
        idShow.setText(getIntent().getStringExtra("MemID"));

        final EditText tName = (EditText) findViewById(R.id.txtName);
        final EditText tBlood = (EditText) findViewById(R.id.txtBlood);
        final EditText tSex = (EditText) findViewById(R.id.txtSex);
        final EditText tPhone = (EditText) findViewById(R.id.txtPhone);
        final EditText tLocation = (EditText) findViewById(R.id.txtCurrentLocation);
        final EditText tEmail = (EditText) findViewById(R.id.txtEmail);

        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        AlertDialog ad = adb.create();

        if(tName.getText().length() == 0)
        {
            ad.setMessage("Please input [Name] ");
            ad.show();
            tName.requestFocus();
            return false;
        }
        if(tBlood.getText().length() == 0)
        {
            ad.setMessage("Please input [Tel] ");
            ad.show();
            tBlood.requestFocus();
            return false;
        }
        if(tSex.getText().length() == 0)
        {
            ad.setMessage("Please input [Tel] ");
            ad.show();
            tSex.requestFocus();
            return false;
        }
        if(tPhone.getText().length() == 0)
        {
            ad.setMessage("Please input [Tel] ");
            ad.show();
            tPhone.requestFocus();
            return false;
        }
        if(tLocation.getText().length() == 0)
        {
            ad.setMessage("Please input [Tel] ");
            ad.show();
            tLocation.requestFocus();
            return false;
        }
        if(tEmail.getText().length() == 0)
        {
            ad.setMessage("Please input [Tel] ");
            ad.show();
            tEmail.requestFocus();
            return false;
        }

        final myDBClass myDb = new myDBClass(this);

        long saveStatus = myDb.UpdateData(MemID,
                tName.getText().toString(),
                tBlood.getText().toString(),
                tSex.getText().toString(),
                tPhone.getText().toString(),
                tLocation.getText().toString(),
                tEmail.getText().toString());
        if(saveStatus <=  0)
        {
            ad.setMessage("Error!! ");
            ad.show();
            return false;
        }

        Toast.makeText(UpdateActivity2.this, "Update Data Successfully. ",
                Toast.LENGTH_SHORT).show();
        return true;
    }

}
