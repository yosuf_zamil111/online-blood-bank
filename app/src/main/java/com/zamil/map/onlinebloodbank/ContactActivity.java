package com.zamil.map.onlinebloodbank;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ContactActivity extends AppCompatActivity {


    String adminEmail ="zamil@gmail.com";
    String adminPass ="1234";
    private Dialog dialog;
    EditText adminemail,adminpass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Button okButton;
        okButton = (Button) findViewById(R.id.adminLogin);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callLoginDialog();
            }
        });
    }


    private void callLoginDialog()
    {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.admin_login);
        dialog.setCancelable(false);


        Button login = (Button) dialog.findViewById(R.id.buttonlogin);
        adminemail = (EditText) dialog.findViewById(R.id.adminLoginEmail);
        adminpass = (EditText) dialog.findViewById(R.id.adminLoginPassord);
        dialog.show();

        login.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                String storeEmail = adminEmail.toString();
                String storePass = adminPass.toString();

                String giveEmail = adminemail.getText().toString();
                String givePass = adminpass.getText().toString();

                if(giveEmail.equals(storeEmail) && givePass.equals(storePass)){

                    Toast.makeText(ContactActivity.this, "Successfully Admin Login",
                            Toast.LENGTH_SHORT).show();
                    Intent out = new Intent(ContactActivity.this,RegisteredDonarList.class);
                    startActivity(out);
                }
                else {
                    Toast.makeText(ContactActivity.this, "Invalid Admin Login",
                            Toast.LENGTH_SHORT).show();
                    Intent out = new Intent(ContactActivity.this,HomeActivity.class);
                    startActivity(out);
                }

            }
        });

    }

}
