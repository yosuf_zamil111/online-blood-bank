package com.zamil.map.onlinebloodbank;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by zamil on 24-Nov-15.
 */
public class myDBClass extends SQLiteOpenHelper {

    public SQLiteDatabase db;


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "bloodbank";
    private static final String TABLE_MEMBER = "tableofblood";

    public myDBClass(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_MEMBER +
                "(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                " Name TEXT(100)," +
                " BloodGroup TEXT(100)," +
                " Sex TEXT(100)," +
                " Phone TEXT(100)," +
                " Location TEXT(100)," +
                " Email TEXT(100)," +
                " Password TEXT(100));");
        Log.d("CREATE TABLE", "Create Table Successfully.");
    }

    public long InsertData(String id, String name, String blood, String sex,String phone,String currentlocation, String email, String pass) {
        try {
            SQLiteDatabase db;
            db = this.getWritableDatabase();
            ContentValues Val = new ContentValues();

            Val.put("ID", id);
            Val.put("Name", name);
            Val.put("BloodGroup", blood);
            Val.put("Sex", sex);
            Val.put("Phone", phone);
            Val.put("Location", currentlocation);
            Val.put("Email", email);
            Val.put("Password", pass);

            long rows = db.insert(TABLE_MEMBER, null, Val);
            db.close();
            return rows;
        } catch (Exception e) {
            return -1;
        }
    }

    public String[] SelectData(String strMemberID) {
        try {
            String arrData[] = null;
            SQLiteDatabase db;
            db = this.getReadableDatabase(); // Read Data
            Cursor cursor = db.query(TABLE_MEMBER, new String[] { "*" },
                    "ID=?",
            new String[] { String.valueOf(strMemberID) }, null, null, null, null);
            if(cursor != null)
            {
                if (cursor.moveToFirst()) {
                    arrData = new String[cursor.getColumnCount()];
                    arrData[0] = cursor.getString(0);
                    arrData[1] = cursor.getString(1);
                    arrData[2] = cursor.getString(2);
                    arrData[3] = cursor.getString(3);
                    arrData[4] = cursor.getString(4);
                    arrData[5] = cursor.getString(5);
                    arrData[6] = cursor.getString(6);
                    //arrData[7] = cursor.getString(7);
                }
            }
            cursor.close();
            db.close();
            return arrData;
        } catch (Exception e) {
            return null;
        }
    }




    public ArrayList<HashMap<String, String>> SelectAllData() {
        try {
            ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;
            SQLiteDatabase db;
            db = this.getReadableDatabase(); // Read Data
            String strSQL = "SELECT  * FROM " + TABLE_MEMBER;
            Cursor cursor = db.rawQuery(strSQL, null);
            if(cursor != null)
            {
                if (cursor.moveToFirst()) {
                    do {
                        map = new HashMap<String, String>();
                       // map.put("MemberID", cursor.getString(0));
                        map.put("BloodGroup", cursor.getString(2));
                        map.put("Location", cursor.getString(5));
                        //map.put("Sex", cursor.getString(3));
                        MyArrList.add(map);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
            db.close();
            return MyArrList;
        } catch (Exception e) {
            return null;
        }
    }

    public ArrayList<HashMap<String, String>> SelectAllData1() {
        try {
            ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;
            SQLiteDatabase db;
            db = this.getReadableDatabase(); // Read Data
            String strSQL = "SELECT  * FROM " + TABLE_MEMBER;
            Cursor cursor = db.rawQuery(strSQL, null);
            if(cursor != null)
            {
                if (cursor.moveToFirst()) {
                    do {
                        map = new HashMap<String, String>();
                        map.put("ID", cursor.getString(0));
                        map.put("BloodGroup", cursor.getString(2));
                        map.put("Location", cursor.getString(5));
                        //map.put("Sex", cursor.getString(3));
                        MyArrList.add(map);
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
            db.close();
            return MyArrList;
        } catch (Exception e) {
            return null;
        }
    }


    public String getSinlgeEntry(String email)
    {
        db = this.getReadableDatabase();
        String query = "select * from "+TABLE_MEMBER;
        Cursor cursor = db.rawQuery(query,null);

        String a,b;
        b = "Sorry Found";
        if(cursor.moveToFirst()){
            do{
                a = cursor.getString(6);
                if(a.equals(email)){

                    b = cursor.getString(7);
                    break;
                }

            }while (cursor.moveToNext());
        }
        return b;
    }



    // Update Data
    public long UpdateData(String strMemberID,String strName,String strBlood,String strSex,String strPhone,String strLocation,String strEmail) {
        try {
            SQLiteDatabase db;
            db = this.getWritableDatabase(); // Write Data
            ContentValues Val = new ContentValues();
            Val.put("Name", strName);
            Val.put("BloodGroup", strBlood);
            Val.put("Sex", strSex);
            Val.put("Phone", strPhone);
            Val.put("Location", strLocation);
            Val.put("Email", strEmail);
           // Val.put("Password", strPass);
            long rows = db.update(TABLE_MEMBER, Val, " ID = ?",
                    new String[] { String.valueOf(strMemberID) });
            db.close();
            return rows; // return rows updated.
        } catch (Exception e) {
            return -1;
        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEMBER);
        onCreate(db);
    }





}
