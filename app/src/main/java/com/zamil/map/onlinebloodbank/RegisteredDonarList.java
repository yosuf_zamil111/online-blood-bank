package com.zamil.map.onlinebloodbank;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class RegisteredDonarList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registered_donar_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final myDBClass myDb = new myDBClass(this);
        final ArrayList<HashMap<String, String>> MebmerList = myDb.SelectAllData1();

        ListView lisView1 = (ListView)findViewById(R.id.listView1);

        SimpleAdapter sAdap;

        sAdap = new SimpleAdapter(RegisteredDonarList.this, MebmerList, R.layout.activity_column1,
                new String[] {"ID","BloodGroup","Location"},
                new int[] {R.id.regid,R.id.bloodgrp,R.id.location});
        lisView1.setAdapter(sAdap);

        lisView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int position, long mylng) {

                Intent newActivity = new Intent(RegisteredDonarList.this, DetailActivity.class);
                newActivity.putExtra("MemID", MebmerList.get(position).get("ID").toString());
                //newActivity.putExtra("MemBlood", MebmerList.get(position).get("BloodGroup").toString());
                //newActivity.putExtra("MemPhone", MebmerList.get(position).get("Phone").toString());
                //newActivity.putExtra("MemPassword", MebmerList.get(position).get("Password").toString());

               startActivity(newActivity);
            }
        });

    }

}
